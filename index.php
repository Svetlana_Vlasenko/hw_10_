<?php 
include_once $_SERVER['DOCUMENT_ROOT'].'/data/product_arr.php';
$items = NULL;
if(!empty($_COOKIE['items'])){
   $items = json_decode($_COOKIE['items'], true);
}
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="Мягкие большие игрушки">
   <?php include_once $_SERVER['DOCUMENT_ROOT'].'/html/header-connects.php' ?> 
   <title>FLUFFY</title>
</head>
<body>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/html/header.php' ?>
   <div class="container">
      <div class="body row justify-content-center ">
         <div class="col-8"> 
            <table class="table table-striped table-hover">
               <tr>
                  <td>Name</td>
                  <td>Price</td>
                  <td>BUY</td>
               </tr>
               <!-- выводим все товры -->
               <?php foreach($products as$key => $product): ?>
                  <tr>  
                     <td><?=$product['title']; ?></td>
                     <td><?=$product['price_val']; ?></td>
                     <td>
                        <form action="/cart_add.php" method="get">
                           <a  href="/card_add.php?buy_item=<?=$key++?>">Buy</a>
                        </form>
                     </td>
                  </tr>
               <?php endforeach; ?>
            </table>
         </div>
      </div>
      <?php if(empty($items)):?>
         <div class="text-center">
            <h1>Basket is empty:</h1>
         </div>
         <?php else:?>
         <div class="text-center">
            <h1>Basket:</h1>
         </div>
         <div class="alert alert-dark" role="alert">
            <div class="row">
               <div class="col-5">
                 <!-- товары и их количество при покупке -->
                  <?php foreach($items as $itemKeys => $item):?>
                     <pre> <?=$products[$itemKeys]['title']?> (<?=$item?>);</pre>
                  <?php endforeach;?>
               </div> 
            </div>
        </div>
      <?php endif ;?>
   <?php include_once $_SERVER['DOCUMENT_ROOT'].'/html/footer.php' ?>     
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>

